# @khaos.ca/config

[![npm][npm-badge]][npm-url]
[![License: MIT][license-badge]][license-url]

> Opinionated configurations.

## Configurations

| config                                                                    | Description                                            |
| ------------------------------------------------------------------------- | ------------------------------------------------------ |
| [commitlint](commitlint-config/core.yaml)                                 | Configurations for commitlint                          |
| [eslint/core](eslint-rules/core.yaml)                                     | Configurations for ESLint - Core                       |
| [eslint/typescript](eslint-rules/typescript.yaml)                         | Configurations for ESLint - TypeScript                 |
| [eslint/browser](eslint-rules/browser.yaml)                               | Configurations for ESLint - Browser                    |
| [eslint/react](eslint-rules/react.yaml)                                   | Configurations for ESLint - React                      |
| [eslint/react/typescript](eslint-rules/react-typescript.yaml)             | Configurations for ESLint - React (TypeScript)         |
| [eslint/next/typescript](eslint-rules/next-typescript.yaml)               | Configurations for ESLint - Next.js (TypeScript)       |
| [eslint/storybook](eslint-rules/storybook.yaml)                           | Configurations for ESLint - Storybook                  |
| [eslint/node/script](eslint-rules/node-script.yaml)                       | Configurations for ESLint - Node.js (CommonJS)         |
| [eslint/node/module](eslint-rules/node-module.yaml)                       | Configurations for ESLint - Node.js (ESM)              |
| [eslint/node/module/typescript](eslint-rules/node-module-typescript.yaml) | Configurations for ESLint - Node.js (ESM - TypeScript) |
| [eslint/jest](eslint-rules/jest.yaml)                                     | Configurations for ESLint - Jest                       |
| eslint/json                                                               | Configurations for ESLint - JSON                       |
| [prettier](prettier-config/core.yaml)                                     | Configurations for Prettier                            |
| [stylelint/core](stylelint-config/core.yaml)                              | Configurations for stylelint - Core                    |
| [stylelint/sass](stylelint-config/sass.yaml)                              | Configurations for stylelint - SASS                    |
| [tsconfig/next](tsconfig-config/next.json)                                | Configurations for TypeScript - Next.js                |

## Install

```
$ npm install @khaos.ca/config
```

## Usage for commitlint

Add some commitlint config to your `commitlint.config.js`:

```js
import config from "@khaos.ca/config/commitlint"

export default {
  ...config,
}
```

## Usage for ESLint

Add some ESLint config to your `eslint.config.js`:

```js
import nextConfig from "@khaos.ca/config/eslint/next/typescript"
import storybookConfig from "@khaos.ca/config/eslint/storybook"

export default [
  {
    name: "IGNORES",
    ignores: ["next-env.d.ts", ".next/*"],
  },

  ...nextConfig,
  ...storybookConfig,

  {
    name: "CONFIG",
    languageOptions: {
      parserOptions: {
        tsconfigRootDir: import.meta.dirname,
      },
    },
  },
]
```

## Usage for Prettier

Add some Prettier config to your `prettier.config.js`:

```js
import config from "@khaos.ca/config/prettier"

export default {
  ...config,
}
```

## Usage for stylelint

Add some stylelint config to your `stylelint.config.js`:

```js
import config from "@khaos.ca/config/stylelint/core"

export default {
  ...config,
}
```

## Usage for TSConfig

Add some TSConfig config to your `tsconfig.json`:

```json
{
  "extends": "@khaos.ca/config/tsconfig/next"
}
```

## Documentation

See the [Changelog](CHANGELOG.md) to see what has changed.

## Contribute

See the [Contributing Guidelines](CONTRIBUTING.md) for ways to get started.

See the [Support Guide](SUPPORT.md) for ways to get help.

See the [Security Policy](SECURITY.md) for sharing vulnerability reports.

This project has a [Code of Conduct](CODE_OF_CONDUCT.md).
By interacting with this repository, organization, or community you agree to abide by its terms.

## License

[MIT](LICENSE) © [khaos.ca](https://khaos.ca/)

[npm-badge]: https://img.shields.io/npm/v/@khaos.ca/config?style=flat-square
[tests-badge]: https://img.shields.io/github/actions/workflow/status/khaos.ca/config/tests.yaml?style=flat-square&branch=main
[license-badge]: https://img.shields.io/badge/license-MIT-green?style=flat-square
[npm-url]: https://www.npmjs.com/package/@khaos.ca/config
[license-url]: https://opensource.org/licenses/MIT
