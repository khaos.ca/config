# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).






## [Unreleased]



## [1.1.1] - 2025-03-03
### Changed
- Change Prettier config



## [1.1.0] - 2025-03-03
### Changed
- Support vanilla JavaScript with TypeScript configs
- Maintenance updates


## [1.0.1] - 2024-11-23
### Changed
- Complete overhaul



## [0.2.0] - 2024-01-27
### Changed
- Update dependencies (commitlint@18)
- Update peerDependencies (next@14, prettier@3)
- Update ESLint ECMAScript version to 2023



## [0.1.0] - 2023-04-19
### Added
- Initial






[Unreleased]: https://gitlab.com/khaos.ca/config/compare/1.1.1...HEAD
[1.1.1]:      https://gitlab.com/khaos.ca/config/compare/1.1.0...1.1.1
[1.1.0]:      https://gitlab.com/khaos.ca/config/compare/1.0.1...1.1.0
[1.0.1]:      https://gitlab.com/khaos.ca/config/compare/0.2.0...1.0.1
[0.2.0]:      https://gitlab.com/khaos.ca/config/compare/0.1.0...0.2.0
[0.1.0]:      https://gitlab.com/khaos.ca/config/releases/tag/0.1.0
