//--------------------------------------------------------
//-- commitlint - Core
//--------------------------------------------------------
import { getConfig } from "../get-config.js"

export default getConfig("commitlint", "core")
