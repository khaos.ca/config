//--------------------------------------------------------
//-- Prettier - Core
//--------------------------------------------------------
import { getConfig } from "../get-config.js"

export default getConfig("prettier", "core")
