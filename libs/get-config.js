//--------------------------------------------------------
//-- Read and process YAML config
//--------------------------------------------------------
import fs from "node:fs"
import path from "node:path"
import { fileURLToPath } from "node:url"
import deepMerge from "deepmerge"
import yaml from "js-yaml"

const DIRECTORIES = {
  commitlint: "commitlint-config",
  eslint: "eslint-rules",
  prettier: "prettier-config",
  stylelint: "stylelint-config",
  tsconfig: "tsconfig-config",
}

export const getConfig = (type, ...paths) => {
  return paths.reduce((config, filePath) => {
    const filePaths = filePath.split("/")
    const filename = filePaths.pop()
    const current = yaml.load(
      fs.readFileSync(
        path.join(
          path.dirname(fileURLToPath(import.meta.url)),
          "..",
          DIRECTORIES[type],
          ...filePaths,
          `${filename}.yaml`,
        ),
        "utf-8",
      ),
    )

    return deepMerge.all([config, current], {
      arrayMerge: (destination, source) => source,
    })
  }, {})
}
