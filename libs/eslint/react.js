//--------------------------------------------------------
//-- ESLint - React
//--------------------------------------------------------
import babelParser from "@babel/eslint-parser"
import eslintPluginImport from "eslint-plugin-import"
import eslintPluginJSXA11y from "eslint-plugin-jsx-a11y"
import eslintPluginReact from "eslint-plugin-react"
import eslintPluginReactHooks from "eslint-plugin-react-hooks"

import { getConfig } from "../get-config.js"
import browser from "./browser.js"

export default [
  eslintPluginImport.flatConfigs.recommended,
  eslintPluginJSXA11y.flatConfigs.recommended,
  {
    name: "react/recommended (forced naming)",
    ...eslintPluginReact.configs.flat.recommended,
  },
  {
    name: "react/jsx-runtime (forced naming)",
    ...eslintPluginReact.configs.flat["jsx-runtime"],
  },
  {
    name: "react/hook (khaos.ca hack for react hook)",
    plugins: {
      "react-hooks": eslintPluginReactHooks,
    },
    rules: {
      ...eslintPluginReactHooks.configs.recommended.rules,
    },
  },

  ...browser,

  {
    name: "@khaos.ca/react",
    settings: {
      react: {
        version: "detect",
      },
    },
    languageOptions: {
      sourceType: "module",
      parser: babelParser,
    },
    ...getConfig("eslint", "react"),
  },
]
