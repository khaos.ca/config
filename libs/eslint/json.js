//--------------------------------------------------------
//-- ESLint - JSON
//--------------------------------------------------------
import eslintPluginJSON from "eslint-plugin-json"

export default [
  {
    name: "@khaos.ca/json",
    files: ["**/*.json"],
    ...eslintPluginJSON.configs.recommended,
  },
]
