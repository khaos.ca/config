//--------------------------------------------------------
//-- ESLint - Browser
//--------------------------------------------------------
import globals from "globals"
import { getConfig } from "../get-config.js"
import core from "./core.js"

export default [
  ...core,

  {
    name: "@khaos.ca/browser",
    languageOptions: {
      globals: {
        ...globals.browser,
      },
    },
    ...getConfig("eslint", "browser"),
  },
]
