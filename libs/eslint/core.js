//--------------------------------------------------------
//-- ESLint - Core
//--------------------------------------------------------
import eslint from "@eslint/js"
import eslintPluginUnicorn from "eslint-plugin-unicorn"
import globals from "globals"
import { getConfig } from "../get-config.js"

export default [
  {
    name: "eslint/recommended (forced naming)",
    ...eslint.configs.recommended,
  },
  eslintPluginUnicorn.configs["flat/recommended"],

  {
    name: "@khaos.ca/core",
    languageOptions: {
      parserOptions: {
        ecmaVersion: "latest",
        sourceType: "script",
        ecmaFeatures: {
          globalReturn: false,
          impliedStrict: false,
          jsx: false,
        },
      },
      globals: {
        ...globals.builtin,
      },
    },
    ...getConfig("eslint", "core"),
  },
]
