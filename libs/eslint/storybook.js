//--------------------------------------------------------
//-- ESLint - Storybook
//--------------------------------------------------------
import { getConfig } from "../get-config.js"

export default [
  {
    name: "@khaos.ca/storybook",
    files: ["**/*.stories.ts", "**/*.stories.tsx"],
    ...getConfig("eslint", "storybook"),
  },
]
