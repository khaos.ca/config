//--------------------------------------------------------
//-- ESLint - Jest
//--------------------------------------------------------
import eslintPluginJest from "eslint-plugin-jest"
import { getConfig } from "../get-config.js"
import nodeModule from "./node-module.js"

export default [
  {
    name: "@khaos.ca/jest",
    ...eslintPluginJest.configs["flat/recommended"],
    ...eslintPluginJest.configs["flat/style"],
    ...getConfig("eslint", "jest"),
  },

  ...nodeModule,
]
