//--------------------------------------------------------
//-- ESLint - Next.js - TypeScript
//--------------------------------------------------------
import eslintPluginNext from "@next/eslint-plugin-next"
import eslintPluginSimpleImportSort from "eslint-plugin-simple-import-sort"
import { getConfig } from "../get-config.js"
import reactTypescript from "./react-typescript.js"
import typescript from "./typescript.js"

export default [
  ...reactTypescript,
  ...typescript,
  {
    name: "@next/next/(khaos.ca hack for recommended & core-web-vitals)",
    rules: {
      ...eslintPluginNext.configs.recommended.rules,
      ...eslintPluginNext.configs["core-web-vitals"].rules,
    },
  },
  {
    name: "@khaos.ca/next/typescript",
    plugins: {
      "@next/next": eslintPluginNext,
      "simple-import-sort": eslintPluginSimpleImportSort,
    },
    ...getConfig("eslint", "next-typescript"),
  },
]
