//--------------------------------------------------------
//-- ESLint - Node.js - Script
//--------------------------------------------------------
import eslintPluginN from "eslint-plugin-n"
import globals from "globals"
import { getConfig } from "../get-config.js"
import core from "./core.js"

export default [
  ...core,

  eslintPluginN.configs["flat/recommended-script"],

  {
    name: "@khaos.ca/node/script",
    languageOptions: {
      globals: {
        ...globals.node,
      },
    },
    ...getConfig("eslint", "node__base", "node-script"),
  },
]
