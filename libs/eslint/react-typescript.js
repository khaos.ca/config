//--------------------------------------------------------
//-- ESLint - React - TypeScript
//--------------------------------------------------------
import eslintPluginImport from "eslint-plugin-import"
import { getConfig } from "../get-config.js"
import react from "./react.js"

export default [
  ...react,

  {
    name: "import/typescript (forced naming)",
    ...eslintPluginImport.configs.typescript,
  },
  {
    name: "@khaos.ca/react/typescript",
    settings: {
      "import/resolver": {
        typescript: {
          alwaysTryTypes: true,
        },
      },
    },
    ...getConfig("eslint", "react-typescript"),
  },
]
