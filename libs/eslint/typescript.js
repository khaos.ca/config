//--------------------------------------------------------
//-- ESLint - TypeScript
//--------------------------------------------------------
import tsESLint from "typescript-eslint"
import { getConfig } from "../get-config.js"

export default [
  ...tsESLint.configs.strictTypeChecked,
  ...tsESLint.configs.stylisticTypeChecked,

  {
    name: "@khaos.ca/typescript",
    languageOptions: {
      parserOptions: {
        projectService: true,
        allowDefaultProject: ["*.ts", "*.tsx"],
      },
    },
    ...getConfig("eslint", "typescript"),
  },
  {
    name: "@khaos.ca/typescript (Vanilla JavaScript exception)",
    files: ["*.js", "*.jsx", "*.cjs", "*.mjs"],
    ...tsESLint.configs.disableTypeChecked,
  },
]
