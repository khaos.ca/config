//--------------------------------------------------------
//-- ESLint - Node.js - Module - TypeScript
//--------------------------------------------------------
import eslintPluginImport from "eslint-plugin-import"
import eslintPluginN from "eslint-plugin-n"
import globals from "globals"
import { getConfig } from "../get-config.js"
import core from "./core.js"

export default [
  ...core,

  eslintPluginN.configs["flat/recommended-module"],
  eslintPluginImport.flatConfigs.recommended,

  {
    name: "import/typescript (forced naming)",
    ...eslintPluginImport.configs.typescript,
  },
  {
    name: "@khaos.ca/node/module/typescript",
    settings: {
      "import/resolver": {
        typescript: {
          alwaysTryTypes: true,
        },
      },
    },
    languageOptions: {
      globals: {
        ...globals.nodeBuiltin,
      },
    },
    ...getConfig("eslint", "node__base", "node-module", "node-module-typescript"),
  },
]
