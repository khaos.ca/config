//--------------------------------------------------------
//-- ESLint - Node.js - Module
//--------------------------------------------------------
import eslintPluginImport from "eslint-plugin-import"
import eslintPluginN from "eslint-plugin-n"
import globals from "globals"
import { getConfig } from "../get-config.js"
import core from "./core.js"

export default [
  ...core,

  eslintPluginN.configs["flat/recommended-module"],
  eslintPluginImport.flatConfigs.recommended,

  {
    name: "@khaos.ca/node/module",
    languageOptions: {
      globals: {
        ...globals.nodeBuiltin,
      },
    },
    ...getConfig("eslint", "node__base", "node-module"),
  },
]
