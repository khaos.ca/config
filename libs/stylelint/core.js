//--------------------------------------------------------
//-- stylelint - Core
//--------------------------------------------------------
import { getConfig } from "../get-config.js"

export default getConfig("stylelint", "__base", "core")
