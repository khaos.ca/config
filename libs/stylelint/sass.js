//--------------------------------------------------------
//-- stylelint - SASS
//--------------------------------------------------------
import { getConfig } from "../get-config.js"

export default getConfig("stylelint", "__base", "sass")
