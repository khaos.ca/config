import eslint from "@eslint/js"
import eslintConfigPrettier from "eslint-config-prettier"
import eslintPluginN from "eslint-plugin-n"
import globals from "globals"

export default [
  eslint.configs.recommended,
  eslintPluginN.configs["flat/recommended"],
  eslintConfigPrettier,
  {
    files: ["**/*.js"],
    languageOptions: {
      parserOptions: {
        ecmaVersion: "latest",
        sourceType: "module",
        ecmaFeatures: {
          globalReturn: false,
          impliedStrict: false,
          jsx: false,
        },
      },
      globals: {
        ...globals.builtin,
        ...globals.nodeBuiltin,
      },
    },
  },
]
